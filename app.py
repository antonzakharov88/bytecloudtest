from flask import Flask, request, render_template, send_from_directory
import os
import time

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        start_time = time.time()
        file = request.files['file']
        file.save(os.path.join('uploads', file.filename))
        duration = time.time() - start_time
        download_link = f'http://70.34.244.238:5000/uploads/{file.filename}'
        return render_template('upload.html', duration=duration, download_link=download_link, file=file)
    return render_template('upload.html')


@app.route('/uploads/<filename>')
def download_file(filename):
    return send_from_directory('uploads', filename, as_attachment=True)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run()
